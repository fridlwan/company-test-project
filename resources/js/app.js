import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap';
import BootstrapVue from 'bootstrap-vue';
import FlashMessage from '@smartweb/vue-flash-message';
import DatePicker from 'vue2-datepicker'
import 'vue2-datepicker/index.css';

Vue.use(BootstrapVue);
Vue.use(FlashMessage);
Vue.component('date-picker', DatePicker)
window.swal = require('sweetalert2');
window.axios = require('axios');

import Multiselect from 'vue-multiselect'
Vue.component('multiselect', Multiselect)
import "vue-multiselect/dist/vue-multiselect.min.css"

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
