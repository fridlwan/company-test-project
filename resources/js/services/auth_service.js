import jwt from 'jsonwebtoken';
import store from '../store';

export function login(user) {
    return axios.post('/api/auth/login', user)
    .then(response => {
        if (response.status == 200) {
            setToken(response.data);
        }
    });
}

export function setToken(user) {
    const token = jwt.sign({ user: user }, 'secretkeyikupon98765432123456789');
    localStorage.setItem('ikupon-token', token);
    store.dispatch('authenticate', user.user);
}

export function isLoggedIn() {
    const token = localStorage.getItem('ikupon-token');
    return token != null;
}

export function logout() {
    axios.get('/api/auth/logout', {
        headers: {
            Authorization: 'Bearer '+getAccessToken()
        }
    });
    localStorage.removeItem('ikupon-token');
}

export function getAccessToken() {
    const token = localStorage.getItem('ikupon-token');
    if (!token) {
        return null;
    }
    
    const tokenData = jwt.decode(token);
    return tokenData.user.access_token;
}

export function getUserScope() {
    const token = localStorage.getItem('ikupon-token');
    if (!token) {
        return null;
    }
    
    const tokenData = jwt.decode(token);
    return tokenData.user.user.scope;
}

export function getProfile() {
    return axios.get('/api/auth/profile', {
        headers: {
            Authorization: 'Bearer '+getAccessToken()
        }
    });
}