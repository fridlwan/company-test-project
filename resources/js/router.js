import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import * as auth from './services/auth_service';

Vue.use(Router);

const routes = [
    {
        path: '/',
        component: Home,
        children: [
            {
                path: 'dashboard',
                name: 'dashboard',
                component: () => import('./views/Dashboard.vue'),
            },
            {
                path: 'companies',
                name: 'companies',
                component: () => import('./views/Companies.vue'),
                beforeEnter: (to, from, next) => {
                    if (auth.getUserScope().includes('crud')) {
                        next();
                    } else {
                        next('/Restricted');
                    }
                }
            },
            {
                path: 'employees',
                name: 'employees',
                component: () => import('./views/Employees.vue'),
                beforeEnter: (to, from, next) => {
                    if (auth.getUserScope().includes('crud')) {
                        next();
                    } else {
                        next('/Restricted');
                    }
                }
            },
            {
                path: 'quotes',
                name: 'quotes',
                component: () => import('./views/Quotes.vue'),
                beforeEnter: (to, from, next) => {
                    if (auth.getUserScope().includes('crud')) {
                        next();
                    } else {
                        next('/Restricted');
                    }
                }
            },
        ],
        beforeEnter: (to, from, next) => {
            if (!auth.isLoggedIn()) {
                next('/login');
            } else {
                next();
            }
        }
    },
  	{
        path: '/login',
        name: 'login',
        component: () => import('./views/authentication/Login.vue'),
        beforeEnter: (to, from, next) => {
            if (!auth.isLoggedIn()) {
                next();
            } else {
                next('/home');
            }
        }
    },
  	{
        path: '*',
        name: 'Restricted',
        component: () => import('./views/Restricted.vue'),
    },
];

const router = new Router({
    mode: 'history',
    routes: routes,
  	linkActiveClass: 'active'
});

export default router;