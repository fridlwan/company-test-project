import Vue from 'vue';
import Vuex from 'vuex';
import * as auth from './services/auth_service';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        isLoggedIn: null,
        apiURL: '/api',
        serverPath: '/',
        profile: {},
        coupon_id: '',
        ikupon_code: '',
    },
    mutations: {
        authenticate(state, payload) {
            state.isLoggedIn = auth.isLoggedIn();
            if (state.isLoggedIn) {
                state.profile = payload;
            } else {
                state.profile = {};
            }
        },
        coupon(state, payload) {
            if (payload) {
                state.coupon_id = payload.id;
                state.ikupon_code = payload.ikupon_code;
            } else {
                state.coupon_id = '';
                state.ikupon_code = '';
            }
        },
    },
    actions: {
        authenticate(context, payload) {
            context.commit('authenticate', payload);
        },
        coupon(context, payload) {
            context.commit('coupon', payload);
        },
    }
});