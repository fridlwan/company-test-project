<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \Illuminate\Auth\AuthenticationException && $request->wantsJson()){
            return response()->json(array(
                "response_code" => 401,
                "response_message" => "Unauthenticated"
            ), 401);
        }
        
        if ($exception instanceof \Laravel\Passport\Exceptions\MissingScopeException && $request->wantsJson()){
            return response()->json(array(
                "response_code" => 403,
                "response_message" => "Forbidden access"
            ), 403);
        }

        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException && $request->wantsJson()){
            return response()->json(array(
                "response_code" => 404,
                "response_message" => "Not found"
            ), 404);
        }
        
        if ($exception instanceof \Illuminate\Validation\ValidationException && $request->wantsJson()){
            return response()->json(array(
                "response_code" => 422,
                "response_message" => "Validation error",
                "errors" => $exception->validator->errors()->toArray()
            ), 422);
        }
        
        return parent::render($request, $exception);
    }
}
