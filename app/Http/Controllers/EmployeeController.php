<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use App\Notifications\NewEmployee;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = new Employee();
        $employees = $employees->with('company');

        if (request()->start_date_range) {
            $employees = $employees->whereDate('created_at', '>=', request()->start_date_range);
        }
        
        if (request()->end_date_range) {
            $employees = $employees->whereDate('created_at', '<=', request()->end_date_range);
        }
        
        if ($keyword_first_name = request()->keyword_first_name) {
            $employees = $employees->where('first_name', 'LIKE', '%' . $keyword_first_name . '%');
        }

        if ($keyword_last_name = request()->keyword_last_name) {
            $employees = $employees->where('last_name', 'LIKE', '%' . $keyword_last_name . '%');
        }

        if ($keyword_email = request()->keyword_email) {
            $employees = $employees->where('email', 'LIKE', '%' . $keyword_email . '%');
        }

        if ($keyword_company = request()->keyword_company) {
            $employees = $employees->whereHas('company', function ($query) use ($keyword_company) {
                            $query->where('name', 'LIKE', '%' . $keyword_company . '%');
                        });

        }

        if (($order = request()->order) && ($by = request()->by)) {
            $employees = $employees->orderBy($order, $by);
        }

        $employees = $employees->paginate(request()->paging);

        return response()->json(array(
            "response_code" => 200,
            "response_message" => "Success",
            "data" => $employees
        ), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'company_id' => 'required',
            'email' => 'email|unique:employees,email,NULL,id,deleted_at,NULL',
            'phone' => 'string'
        ]);

        $employee = Employee::create($request->all());

        Notification::send($employee->company, new NewEmployee($employee));
  
        return response()->json(array(
            "response_code" => 201,
            "response_message" => "Employee created successfully",
            "data" => $employee
        ), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'company_id' => 'required',
            'email' => 'email|unique:employees,email, ' . $employee->id . ',id,deleted_at,NULL',
            'phone' => 'string'
        ]);

        $employee->update($request->all());

        return response()->json(array(
            "response_code" => 200,
            "response_message" => "Employee updated successfully",
            "data" => $employee
        ), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();

        return response()->json(array(
            "response_code" => 200,
            "response_message" => "Employee deleted successfully"
        ), 200);
    }
}
