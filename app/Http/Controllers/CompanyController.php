<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = new Company();
        
        if ($keyword = request()->keyword) {
            $companies = $companies->where(function ($query) use ($keyword) {
                                    $query->where('name', 'like', '%' . $keyword . '%')
                                        ->orWhere('email', 'like', '%' . $keyword . '%')
                                        ->orWhere('website', 'like', '%' . $keyword . '%');
                                });                                
        }

        if (($order = request()->order) && ($by = request()->by)) {
            $companies = $companies->orderBy($order, $by);
        }

        if (request()->master) {
            $companies = $companies->get();
        } else {
            $companies = $companies->paginate(request()->paging);
        }

        return response()->json(array(
            "response_code" => 200,
            "response_message" => "Success",
            "data" => $companies
        ), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'email|unique:companies,email,NULL,id,deleted_at,NULL',
            'website' => 'url',
            'logo' => 'image'
        ]);

        $company = Company::create($request->except('logo'));

        if ($request->hasFile('logo')) {
            $path = $request->file('logo')->store('images');

            $company->logo = $path;
            $company->save();
        }
        
        return response()->json(array(
            "response_code" => 201,
            "response_message" => "Company created successfully",
            "data" => $company
        ), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'email|unique:companies,email, ' . $company->id . ',id,deleted_at,NULL',
            'website' => 'url',
            'image' => 'image'
        ]);

        if ($request->hasFile('image')) {
            $request->validate([
                'image' => 'image|mimes:png'
            ]);
        }

        $company->update($request->except('logo'));

        if ($request->hasFile('logo')) {
            if ($old_path = $company->logo) {
                Storage::delete($old_path);
            }
            
            $path = $request->file('logo')->store('images');

            $company->logo = $path;
            $company->save();
        }

        return response()->json(array(
            "response_code" => 200,
            "response_message" => "Company updated successfully",
            "data" => $company
        ), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();

        return response()->json(array(
            "response_code" => 200,
            "response_message" => "Company deleted successfully"
        ), 200);
    }
}
