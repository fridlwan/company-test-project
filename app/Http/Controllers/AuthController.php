<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{   
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        if (!Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return response()->json([
                'response_code' => 401,
                'response_message' => 'Invalid email / password',
            ], 401);
        }

        $user = $request->user();

        $tokenData = $user->createToken('Personal Access Token', explode(" ", $user->scope));

        $token = $tokenData->token;

        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addYear();
        } else {
            $token->expires_at = Carbon::now()->addDays(1);
        }

        if ($token->save()) {
            return response()->json([
                'response_code' => 200,
                'response_message' => 'Logged in successfully',
                'user' => $user,
                'access_token' => $tokenData->accessToken,
                'token_type' => 'Bearer',
                'token_scope' => $tokenData->token,
                'expires_at' => Carbon::parse($tokenData->token->expires_at)->toDateTimeString()
            ], 200); 
        } else {
            return response()->json([
                'response_code' => 500,
                'response_message' => 'Some error occured, Please try again'
            ], 500);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'response_code' => 200,
            'response_message' => 'Logged out successfully!'
        ], 200);
    }
    
    public function profile(Request $request)
    {
        if ($request->user()) {
            return response()->json($request->user(), 200);
        }

        return response()->json([
            'response_code' => 500,
            'response_message' => 'Not logged in!'
        ], 500);
    }
}
