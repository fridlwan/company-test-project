<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use Notifiable;
    use SoftDeletes;
    
    protected $guarded = [];
    
    public function employees()
    {
        return $this->hasMany(Employee::class);    
    }
}
