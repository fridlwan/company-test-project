<?php

use Illuminate\Http\Request;

Route::group(['prefix' => 'auth'], function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', 'AuthController@logout');
        Route::get('profile', 'AuthController@profile');
    });
});

Route::resource('companies', 'CompanyController')->middleware('client:crud-company');
Route::resource('employees', 'EmployeeController')->middleware('client:crud-employee');
Route::get('quotes', 'QuoteController@index')->middleware('client:crud-employee');
